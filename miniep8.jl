## Gabriel P S Piedade
## nUSP 8927356


function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

# exercicio 1

function compareByValue(x,y)
    letras = Vector{Char}("JQKA")
    valores = ["11","12","13","14"]
    conta = 0
    for l in letras
        conta += 1
        if l == x[1]
            x = valores[conta] * x[2]  
        end
        if l == y[1]
            y = valores[conta] * y[2]
        end
    end
    if parse(Int,x[1:length(x)-1]) < parse(Int,y[1:length(y)-1])
        return true
    else
        return false
    end
end

function insercao_1(v)
    tam = length(v)
    for i in 2:tam
        j = i
        while j > 1
            if compareByValue(v[j], v[j - 1]) 
                troca(v, j, j - 1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
end

# exercicio 2

function compareByValueAndSuit(x, y)
    naipes = Vector{Char}("♦♠♥♣")
    conta = 0
    naipe_x = 0
    naipe_y = 0
    for i in naipes
        conta += 1
        if i == x[length(x)]
            naipe_x = conta
        end
        if i == y[length(y)]
            naipe_y  = conta
        end
    end
    if naipe_x < naipe_y || compareByValue(x,y) 
        return true
    else
        return false
    end
end

function insercao_2(v)
    tam = length(v)
    for i in 2:tam
        j = i
        while j > 1
            if compareByValueAndSuit(v[j], v[j - 1]) 
                troca(v, j, j - 1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
end

using Test
function testes()
    
    @test compareByValue("3♠","10♣")
    @test compareByValue("J♥","A♠")
    @test compareByValue("2♠","K♦")
    @test !compareByValue("5♥","5♦")
    
    @test insercao_1(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]) == ["10♥","10♦","J♠","K♠","A♠","A♠"]
    
    @test compareByValueAndSuit("2♠", "A♠")
    @test !compareByValueAndSuit("K♥", "10♥")
    @test compareByValueAndSuit("10♠", "10♥")
    @test compareByValueAndSuit("A♠", "2♥")
    
    @test insercao_2(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]) == ["10♦","J♠","K♠","A♠","A♠","10♥"]
     
end
